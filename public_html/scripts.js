//KLASA MAP - przechowuje całą mapę
function Map(width, height) {
    this.width = width;
    this.height = height;
    this.map = [];
    for (var i = 0; i < this.width; i++) {
        this.map[i] = [];
    }
}
//zwraca fragment mapy do przekazania dla Screen.takeMapPiece()
Map.prototype.getMapPiece = function(fromW, fromH, width, height) {
        var mapPiece = [];
        for (var i = 0; i < width; i++) {
            mapPiece[i] = [];
        }
        for (var i = 0; i < width; i++) {
            for (var j = 0; j < height; j++) {
                mapPiece[i][j] = this.map[fromW + i][fromH + j];
            }
        }
        return mapPiece;
    };
    // dodaje mapField do Map.map[posW][posH]
Map.prototype.addMapField = function(mapField, posW, posH) {
    this.map[posW][posH] = mapField;
};
Map.prototype.setMap = function(outerMap) {
        this.width = outerMap.length;
        this.height = outerMap[0].length;
        for (var i = 0; i < this.width; i++) {
            for (var j = 0; j < this.height; j++) {
                switch (outerMap[i][j]) {
                    case "path":
                        this.map[i][j] = new Ground("path");
                        break;
                    case "green":
                        this.map[i][j] = new Ground("green");
                        break;
                    case "rocks":
                        this.map[i][j] = new Ground("rocks");
                        break;
                    case "water":
                        this.map[i][j] = new Ground("water");
                        break;
                }
            }
        }
    };
    //KLASA MAPFIELD - przechowuje jedno pole mapy, które może zawierać... (reszta w środku)
function MapField() {
    this.ground = null; // podłoże (ziemia, piasek, skały, woda)
    this.creature = null; // postać lub potworek
    this.object = null; // jakieś krzaczki itp
}
//Sprawdza, czy da się wejść na dane pole (nie da się jeśli this.ground:water albo istnieje this.object)
MapField.prototype.canIEnter = function() {
        if (this.ground.type !== "water" && (this.object === null || (this.object !== null && this.object.name === "wave")) && this.creature === null) return true;
        else return false;
    };
    //KLASA GROUND - przechowuje rodzaj gruntu: path, green, sand, rocks, water
function Ground(type) {
    this.type = type;
    switch (this.type) {
        case "path":
            this.image = "src/ground-path-img.png";
            break;
        case "green":
            this.image = "src/ground-green-img.png";
            break;
        case "sand":
            this.image = "src/ground-sand-img.png";
            break;
        case "rocks":
            this.image = "src/ground-rocks-img.png";
            break;
        case "water":
            this.image = "src/ground-water-img.png";
            break;
        default:
            throw "Podano zly rodzaj gruntu";
    }
}
//KLASA CREATURE - DO ZROBIENIA!
function Creature(type, image, posW, posH) {
    this.type = type;
    this.image = image;
    this.posW = posW;
    this.posH = posH;
    this.div = null;
    this.imgObj = null;
    this.barDiv = null;
    this.maxHealth = 100;
    this.health = this.maxHealth;
    this.damage = 10;
    this.experience = 50;
    this.id = Math.floor(Math.random() * 100000);
    this.moveTries = 0;
}
Creature.prototype.addHealthBar = function() {
    var bar = document.createElement("DIV");
    bar.setAttribute("class", "healthBar");
    var barCurrent = document.createElement("DIV");
    barCurrent.setAttribute("class", "healthBarCurrent");
    barCurrent.style.width = (this.health / this.maxHealth) * 24 + "px";
    bar.appendChild(barCurrent);
    this.div.appendChild(bar);
    this.barDiv = bar;
};
Creature.prototype.removeC = function() {
    map.map[this.posW][this.posH].creature = null;
    hero.experience += this.experience;
    hero.printPrompt("+" + this.experience + " exp!", "white", "black");
    if (hero.atkTarget === this) hero.atkTarget = null;
};
Creature.prototype.move = function(dirW, dirH) {
    if (this.moveTries > 10) return true;
    this.moveTries++;
        if (dirW !== 0) {
            if ((map.map[this.posW + dirW][this.posH].object !== null && map.map[this.posW + dirW][this.posH].object.name !== "wave") ||
                map.map[this.posW + dirW][this.posH].ground.type === "water" ||
                map.map[this.posW + dirW][this.posH].creature !== null ||
                (this.posW + dirW === hero.posW && this.posH === hero.posH)) {
                console.log("zajęte");
                return false;
            }
            var self = this;
            self.posW += dirW;
            map.map[self.posW][self.posH].creature = "zamiar";
            function callbackF(self) {
                map.map[self.posW - dirW][self.posH].creature = "flagWasHere";
                map.map[self.posW][self.posH].creature = self;
            }
            if (dirW > 0) {
                $(this.imgObj).animate({
                    top: "32px"
                }, 475, "linear", function() {
                    callbackF(self);
                });
                $(this.barDiv).animate({
                    marginTop: "32px"
                }, 475, "linear", function() {
                    callbackF(self);
                });
            } else {
                $(this.imgObj).animate({
                    top: "-32px"
                }, 475, "linear", function() {
                    callbackF(self);
                });
                $(this.barDiv).animate({
                    marginTop: "-32px"
                }, 475, "linear", function() {
                    callbackF(self);
                });
            }
        }
    if (dirH !== 0) {
        if ((map.map[this.posW][this.posH + dirH].object !== null && map.map[this.posW][this.posH + dirH].object.name !== "wave") ||
            map.map[this.posW][this.posH + dirH].ground.type === "water" ||
            map.map[this.posW][this.posH + dirH].creature !== null ||
            (this.posW === hero.posW && this.posH + dirH === hero.posH)) {
            console.log("zajęte");
            return false;
        }
        var self = this;
        self.posH += dirH;
        map.map[self.posW][self.posH].creature = "zamiar";
        function callbackF2(self) {
            map.map[self.posW][self.posH].creature = self;
            map.map[self.posW][self.posH - dirH].creature = "flagWasHere";
        }
        if (dirH > 0) {
            $(this.imgObj).animate({
                left: "32px"
            }, 475, "linear", function() {
                callbackF2(self);
            });
            $(this.barDiv).animate({
                marginLeft: "32px"
            }, 475, "linear", function() {
                callbackF2(self);
            });
        } else {
            $(this.imgObj).animate({
                left: "-32px"
            }, 475, "linear", function() {
                callbackF2(self);
            });
            $(this.barDiv).animate({
                marginLeft: "-32px"
            }, 475, "linear", function() {
                callbackF2(self);
            });
        }
    }
    return true;
};
Creature.prototype.followHero = function() {
    this.moveTries = 0;
    if (Math.abs(this.posW - hero.posW) <= 1 && Math.abs(this.posH - hero.posH) <= 1) {
        console.log("jestem blisko, nie musze sie ruszac");
        return;
    }
    if (Math.abs(this.posH - hero.posH) === Math.abs(this.posW - hero.posW)) {
        if (!(Math.abs(this.posH - hero.posH) <= 1)) {
            if (Math.random() < 0.5) {
                if (this.posW - hero.posW < 0) {
                    if (!this.move(1, 0)) this.move(0, ((Math.random() < 0.5) ? 1 : -1));
                } else {
                    if (!this.move(-1, 0)) this.move((Math.random() < 0.5) ? 1 : -1, 0);
                }
            } else {
                if (this.posH - hero.posH < 0) {
                    if (!this.move(0, 1)) this.move(((Math.random() < 0.5) ? 1 : -1), 0);
                } else {
                    if (!this.move(0, -1)) this.move(((Math.random() < 0.5) ? 1 : -1), 0);
                }
            }
        }
    } else if (Math.abs(this.posH - hero.posH) > Math.abs(this.posW - hero.posW)) {
        var dirH = ((hero.posH - this.posH) > 0) ? 1 : -1;
        if (!this.move(0, dirH)) {
            var newDirW = (hero.posW - this.posW > 0) ? 1 : -1;
            if (hero.posW - this.posW === 0) newDirW = (Math.random() < 0.5) ? 1 : -1;
            if (!this.move(newDirW, 0)) this.move(-newDirW, 0);
        }
    } else if (Math.abs(this.posH - hero.posH) < Math.abs(this.posW - hero.posW)) {
        var dirW = ((hero.posW - this.posW) > 0) ? 1 : -1;
        if (!this.move(dirW, 0)) {
            var newDirH = (hero.posH - this.posH >= 0) ? 1 : -1;
            if (hero.posH - this.posH === 0) newDirH = (Math.random() < 0.5) ? 1 : -1;
            if (!this.move(0, newDirH)) this.move(0, -newDirH);
        }
    }
};
Creature.prototype.atkHeroF = function() {
    if (Math.abs(hero.posH - this.posH) > 1 || Math.abs(hero.posW - this.posW) > 1) return;
    var dmg = Math.floor(Math.random() * this.damage) - hero.armor;
    if (dmg <= 0) dmg = 0;
    hero.currentHealth -= dmg;
    if (dmg > 0) {
        hero.damageTakenInTurn += dmg;
    } else {
        hero.printPrompt("x", "grey", "lightgrey");
    }
};
Creature.prototype.printPrompt = function(prompt, color, border) {
        //najpierw musimy stworzyć css-ową animację:
        if (this.lastAnimation) this.lastAnimation.remove();
        var cssAnimation = document.createElement('style');
        cssAnimation.type = "text/css";
        var animationKeyframe = document.createTextNode("@keyframes creaturePromptA {" +
            "from { top: " + ((this.posW - screen.posW) * 32 + 20) + "px; font-size: 12px;" +
            "} to { top: " + ((this.posW - screen.posW) * 32 + 10) + "px; font-size: 10px;" +
            "}" +
            "}");
        cssAnimation.appendChild(animationKeyframe);
        document.getElementsByTagName("HEAD")[0].appendChild(cssAnimation);
        this.lastAnimation = cssAnimation;
            //udało się
        var promptDiv = document.createElement("DIV");
        promptDiv.setAttribute("class", "creaturePrompt");
        promptDiv.innerHTML = prompt;
        promptDiv.style.left = (this.posH - screen.posH + 1) * 32 + "px";
        promptDiv.style.color = color;
        promptDiv.style.textShadow = "-1px 0 " + border + ", 0 1px " + border + ", 1px 0 " + border + ",0 -1px " + border;
        promptDiv.style.animationName = "creaturePromptA";
        promptDiv.style.animationDuration = "0.5s";
        promptDiv.style.webkitAnimationName = "creaturePromptA";
        promptDiv.style.webkitAnimationDuration = "0.5s";
        document.getElementsByTagName("body")[0].appendChild(promptDiv);

    };
    //KLASA OBJECT - przechowuje nazwę przedmiotu, jego obrazek
function Object(name, image) {
    this.name = name;
    this.image = image;
    this.next = null;
}
//KLASA HERO - przechowuje dane nt bohatera
// będzie też przechowywać jego ekwipunek
function Hero(posW, posH) {
    this.posH = posH;
    this.posW = posW;
    this.image = "src/hero-img.png";
    this.addToScreen();
    this.level = 1;
    this.experience = 0;
    this.health = 500;
    this.currentHealth = this.health;
    this.mana = 500;
    this.currentMana = this.mana;
    this.damageTakenInTurn = 0;
    this.equipment = new Equipment();
    this.armor = 1 * this.level;
    this.damage = 50 * this.level / 5;
    this.atkTarget = null;
}
Hero.prototype.addToScreen = function() {
    var hero = document.createElement("DIV");
    hero.setAttribute("class", "hero");
    hero.setAttribute("id", "hero");
    var heroImg = document.createElement("IMG");
    heroImg.setAttribute("src", this.image);
    heroImg.setAttribute("id", "heroImg");
    heroImg.setAttribute("class", "hero-img");
    hero.appendChild(heroImg);
    document.getElementById("screen").appendChild(hero);
};
Hero.prototype.printHealth = function() {
    document.getElementById("currentHp").style.width = 256 * this.currentHealth / this.health + "px";
    document.getElementById("currentHpP").innerHTML = Math.floor(this.currentHealth) + " | " + this.health;
};
Hero.prototype.printMana = function() {
    document.getElementById("currentMana").style.width = 256 * this.currentMana / this.mana + "px";
    document.getElementById("currentManaP").innerHTML = Math.floor(this.currentMana) + " | " + this.mana;
};
Hero.prototype.move = function(where) {
    if (this.currentHealth < 0) throw "dead";
    if (where === "up") {
        screen.doIMove = true;
        screen.whereIMove = where;
        return;
    }
    if (where === "down") {
        screen.doIMove = true;
        screen.whereIMove = where;
        return;
    }
    if (where === "left") {
        screen.doIMove = true;
        screen.whereIMove = where;
        return;
    }
    if (where === "right") {
        screen.doIMove = true;
        screen.whereIMove = where;
        return;
    }
    console.log("Hero moved " + where + " . [posW: " + this.posW + "|posH: " + this.posH);
};
Hero.prototype.atkTargetF = function() {
    if (this.currentHealth < 0) throw "dead";
    if (!this.atkTarget) return;
    var targetDiv = document.getElementById(this.atkTarget.id);
    if (!targetDiv) return;
    targetDiv.style.height = "30px";
    targetDiv.style.width = "30px";
    targetDiv.style.border = "0.5px solid red";
    if (Math.abs(this.atkTarget.posH - this.posH) > 1 || Math.abs(this.atkTarget.posW - this.posW) > 1) return;
    var dmg = Math.floor(Math.random() * this.damage);
    this.atkTarget.health -= dmg;
    if (this.atkTarget.health < 0) {
        this.atkTarget.health = 0;
        this.atkTarget.barDiv.remove();
        document.getElementById(this.atkTarget.id).setAttribute("src", "src/blood-img.png");
        this.atkTarget.printPrompt("crit!", "red", "white");
        this.atkTarget = null;
        return;
    }
    if (dmg !== 0) {
        this.atkTarget.printPrompt(dmg, "red", "white");
    } else {
        this.atkTarget.printPrompt("x", "grey", "lightgrey");
    }
};
Hero.prototype.updateLevel = function() {
    var expToNextLvl = Math.pow(this.level, 2) * 100;
    if (this.experience >= expToNextLvl) {
        this.level++;
            this.printPrompt("LVL UP!", "yellow", "black");
        this.health = 250 * this.level / 5;
        this.currentHealth = this.health;
        this.armor = 2 * this.level + this.equipment.armorValue;
        this.damage = 50 * this.level / 5 + this.equipment.attackValue;
    }
};
Hero.prototype.die = function() {
    this.image = "src/blood-img.png";
    $('#hero').empty();
    this.addToScreen();
    document.getElementById("currentHpP").innerHTML = "YOU ARE DEAD";
    document.getElementById("currentHp").style.background = "transparent";
    document.getElementById("currentHp").style.border = 0;
    document.getElementById("screen").style.opacity = 0.6;
    document.getElementById("fullHp").style.background = "black";
};
Hero.prototype.printPrompt = function(prompt, color, border) {
    var promptDiv = document.createElement("DIV");
    if (prompt === "LVL UP!" || prompt.toString().indexOf("xp") > 0) promptDiv.setAttribute("class", "promptImportant");
    else promptDiv.setAttribute("class", "prompt");
    promptDiv.innerHTML = prompt;
    promptDiv.style.color = color;
    promptDiv.style.textShadow = "-1px 0 " + border + ", 0 1px " + border + ", 1px 0 " + border + ",0 -1px " + border;
    document.getElementsByTagName("body")[0].appendChild(promptDiv);
};
Hero.prototype.printInfo = function() {
    var infoDiv = document.getElementById("heroInfo");
    infoDiv.innerHTML = "<p class='left-info'>Poziom:</p><p class='right-info'>" + this.level + "</p><br>" +
        "<p class='left-info'>Doświadczenie:</p><p class='right-info'>" + this.experience + "</p><br>" +
        "<p class='left-info'>Do nast. poziomu: </p><p class='right-info'>" + (Math.pow(this.level, 2) * 100 - this.experience) + "</p><br>" +
        "<p class='left-info'>Max. obrażenia: </p><p class='right-info'>" + this.damage + "</p><br>" +
        "<p class='left-info'>Pancerz: </p><p class='right-info'>" + this.armor + "</p>";
};
Hero.prototype.clearPrompts = function() {
        var prompts = document.getElementsByClassName("prompt");
        for (var i = 0; i < prompts.length; i++) {
            prompts[i].remove();
        }
        var prompts = document.getElementsByClassName("creaturePrompt");
        for (var i = 0; i < prompts.length; i++) {
            prompts[i].remove();
        }
        var prompts = document.getElementsByClassName("promptImportant");
        for (var i = 0; i < prompts.length; i++) {
            prompts[i].remove();
        }
    };
    //KLASA EQUIPMENT - przechowuje przedmioty posiadane przez bohatera
function Equipment() {
    this.attackValue = 0;
    this.armorValue = 0;
    this.helmet = new EquipmentPart("helmet", "src/eq-helmet-img.png", 1, 0);
    this.armor = new EquipmentPart("armor", "src/eq-armor-img.png", 1, 0);
    this.weapon = new EquipmentPart("weapon", "src/eq-weapon-img.png", 0, 1);
    this.shield = null;
    this.boots = new EquipmentPart("boots", "src/eq-boots-img.png", 1, 0);
}
Equipment.prototype.addToScreen = function() {
        document.getElementById("equipment").innerHTML = "";
        this.helmet.addToScreen();
        this.weapon.addToScreen();
        this.armor.addToScreen();
        this.boots.addToScreen();
    };
    //KLASA EQUIPMENTPART - przechowuje części ekwipunku (hełm, pancerz, broń itp)
function EquipmentPart(type, image, armor, attack) {
    this.type = type;
    this.image = image;
    this.armorValue = armor;
    this.attackValue = attack;
}
EquipmentPart.prototype.addToScreen = function() {
    var heroDiv = document.getElementById("hero");
    var eqRightDiv = document.getElementById("equipment");
    if (this.type === "armor") {
        var newDiv = document.createElement("DIV");
        var newDivRight = document.createElement("IMG");
        newDivRight.setAttribute("src", this.image);
        newDiv.setAttribute("class", "armor");
        newDiv.style.backgroundImage = "url('" + this.image + "')";
        heroDiv.appendChild(newDiv);
        eqRightDiv.appendChild(newDivRight);
        return;
    }
    if (this.type === "helmet") {
        var newDiv = document.createElement("DIV");
        var newDivRight = document.createElement("IMG");
        newDivRight.setAttribute("src", this.image);
        newDiv.setAttribute("class", "helmet");
        newDiv.style.backgroundImage = "url('" + this.image + "')";
        heroDiv.appendChild(newDiv);
        eqRightDiv.appendChild(newDivRight);
        return;
    }
    if (this.type === "weapon") {
        var newDiv = document.createElement("DIV");
        var newDivRight = document.createElement("IMG");
        newDivRight.setAttribute("src", this.image);
        newDiv.setAttribute("class", "weapon");
        newDiv.style.backgroundImage = "url('" + this.image + "')";
        heroDiv.appendChild(newDiv);
        eqRightDiv.appendChild(newDivRight);
        return;
    }
    if (this.type === "boots") {
        var newDiv = document.createElement("DIV");
        newDiv.setAttribute("class", "boots");
        var newDivRight = document.createElement("IMG");
        newDivRight.setAttribute("src", this.image);
        newDiv.style.backgroundImage = "url('" + this.image + "')";
        heroDiv.appendChild(newDiv);
        eqRightDiv.appendChild(newDivRight);
        return;
    }
    throw "Nie uwzgledniono w funkcji EquipmentPart.addToScreen()";
};
EquipmentPart.prototype.putEquipmentPartOn = function() {
        switch (this.type) {
            case "helmet":
                hero.equipment.helmet = this;
                break;
            case "armor":
                hero.equipment.armor = this;
                break;
            case "weapon":
                hero.equipment.weapon = this;
                break;
            case "boots":
                hero.equipment.boots = this;
                break;
            default:
                throw "Nie uwzgledniono w funkcji EquipmentPart.putEquipmentPartOn()";
        }
        hero.equipment.attackValue += this.attackValue;
        hero.equipment.armorValue += this.armorValue;
        hero.damage += this.attackValue;
        hero.armor += this.armorValue;
        this.addToScreen();
    };
    //KLASA SCREEN - przechowuje fragment mapy do wyświetlenia
function Screen(width, height) {
    this.width = width;
    this.height = height;
    this.posW = 1;
    this.posH = 1;
    this.creatureTab = [];
    this.screen = [];
    for (var i = 0; i < this.width; i++) {
        this.screen[i] = [];
    }
    this.whereIMoved = "";
    this.whereIMove = "";
    this.doIMove = false;
}
Screen.prototype.findCreature = function(id) {
    for (var i = 0; i < this.creatureTab.length; i++) {
        if (this.creatureTab[i].id == id) {
            return this.creatureTab[i];
        }
    }
    console.log('nie znalazlem');
};
Screen.prototype.addCreatureListeners = function() {
    var self = this;
    $('.creature').bind("click", function() {
        $(this).css("border", "1px solid red");
        $(this).css("height", "30px");
        $(this).css("width", "30px");
        var id = $(this).attr("id");
        hero.atkTarget = self.findCreature(id);
    });
};
Screen.prototype.removeDeadCreatures = function() {
        var self = this;
        $('.creature').each(function() {
            var id = $(this).attr("id");
            var creature = self.findCreature(id);
            if (creature.health <= 0) {
                creature.removeC();
            }
        });
    };
    //dostaje fragment mapy - musi dostać tablicę dwuwymiarową o takich wymiarach jak this.screen
Screen.prototype.takeMapPiece = function(mapPiece) {
        for (var i = 0; i < this.width; i++) {
            for (var j = 0; j < this.height; j++) {
                this.screen[i][j] = mapPiece[i][j];
            }
        }
    };
    //wyświetla fragment mapy na ekranie
Screen.prototype.printScreen = function() {
    this.cleanScreen();
    this.creatureTab = [];
    var creatureCounter = 0;
    $('.screen').css("top", "0px");
    $('.screen').css("left", "0px");
    $(".healthBar").remove();
    for (var i = 0; i < this.width; i++) {
        for (var j = 0; j < this.height; j++) {
            var fieldDiv = document.createElement("DIV");
            fieldDiv.setAttribute("class", "field");
            fieldDiv.setAttribute("id", "field" + i + "" + j);
            fieldDiv.style.top = 32 + i * 32 + "px";
            fieldDiv.style.left = 32 + j * 32 + "px";
            fieldDiv.style.background = "url('" + this.screen[i][j].ground.image + "')";
            if (this.screen[i][j].object !== null && this.screen[i][j].ground.type !== "water") {
                var obj = document.createElement("IMG");
                obj.setAttribute("src", this.screen[i][j].object.image);
                if (this.screen[i][j].object.name === "tree") {
                    obj.setAttribute("class", "object-tree");
                } else {
                    obj.setAttribute("class", "object");
                }
                fieldDiv.appendChild(obj);
                if (this.screen[i][j].object.next !== null && this.screen[i][j].ground.type !== "water") {
                    var obj2 = document.createElement("IMG");
                    obj2.setAttribute("class", "object2");
                    obj2.setAttribute("src", this.screen[i][j].object.next.image);
                    fieldDiv.appendChild(obj2);
                }
            }
            //dodanie potworków
            if (this.screen[i][j].creature !== null && this.screen[i][j].creature !== "zamiar" && this.screen[i][j].creature !== "flagWasHere" && this.screen[i][j].creature.health > 0) {
                var creature = document.createElement("IMG");
                creature.setAttribute("class", "creature");
                creature.setAttribute("id", this.screen[i][j].creature.id);
                creature.setAttribute("src", this.screen[i][j].creature.image);
                fieldDiv.appendChild(creature);
                this.screen[i][j].creature.div = fieldDiv;
                this.screen[i][j].creature.imgObj = creature;
                this.creatureTab[creatureCounter] = this.screen[i][j].creature;
                this.screen[i][j].creature.addHealthBar();
                creatureCounter++;
            }
            document.getElementById("screen").appendChild(fieldDiv);
        }
    }
};
Screen.prototype.cleanScreen = function() {
    $('.screen').empty();
};
Screen.prototype.printSmallMap = function() {
        $('div[id*="map"]').remove();
        for (var i = 0; i < this.width; i++) {
            for (var j = 0; j < this.height; j++) {
                var mapPixel = document.createElement("DIV");
                mapPixel.setAttribute("id", "map" + i + j);
                document.getElementById("smallMap").appendChild(mapPixel);
                switch (this.screen[i][j].ground.type) {
                    case "water":
                        mapPixel.style.background = "blue";
                        break;
                    case "green":
                        if (this.screen[i][j].object && this.screen[i][j].object.name !== "wave") mapPixel.style.background = "green";
                        else mapPixel.style.background = "limegreen";
                        break;
                    case "path":
                        mapPixel.style.background = "darkgrey";
                        break;
                }
            }
        }
    };
    //Przesuwają ekran
Screen.prototype.moveUp = function() {
    $('.screen').animate({
        top: "32px"
    }, 485, "linear", function() {
        return;
    });
    $('.creaturePrompt').animate({
        marginTop: "32px"
    }, 485, "linear", function() {
        return;
    });
};
Screen.prototype.moveDown = function() {
    $('.screen').animate({
        top: "-32px"
    }, 485, "linear", function() {
        return;
    });
    $('.creaturePrompt').animate({
        marginTop: "-32px"
    }, 485, "linear", function() {
        return;
    });
};
Screen.prototype.moveLeft = function() {
    $('.screen').animate({
        left: "32px"
    }, 485, "linear", function() {
        return;
    });
    $('.creaturePrompt').animate({
        left: "+=32"
    }, 485, "linear", function() {
        return;
    });
};
Screen.prototype.moveRight = function() {
    $('.screen').animate({
        left: "-32px"
    }, 485, "linear", function() {
        return;
    });
    $('.creaturePrompt').animate({
        left: "-=32"
    }, 485, "linear", function() {
        return;
    });
};

//-------------------//
//-- TWORZYMY GRĘ! --//
//-------------------//

var map = new Map(30, 30);
var screen = new Screen(10, 10);
var hero = new Hero(4, 5);
hero.equipment.helmet.putEquipmentPartOn();
hero.equipment.weapon.putEquipmentPartOn();
hero.equipment.armor.putEquipmentPartOn();
hero.equipment.boots.putEquipmentPartOn();
    // tworzymy mapkę
for (var i = 0; i < map.width; i++) {
    for (var j = 0; j < map.height; j++) {
        map.map[i][j] = new MapField();
        map.map[i][j].ground = (i < 4 || j < 5 || i > 23 || j > 24) ?
            new Ground("water") : (i < 6 || j < 7 || i > 21 || j > 22) ?
            new Ground("green") : ((i > 16 && i < 19) || (j > 17 && j < 20)) ?
            new Ground("path") : new Ground("green");
            //Fale...
        if (i > 3 && i < 24 && j === 5) {
            if (map.map[i][j].object) map.map[i][j].object.next = new Object("wave", "src/object-wave-left.png");
            if (!map.map[i][j].object) map.map[i][j].object = new Object("wave", "src/object-wave-left.png");
        }
        if (j > 4 && j < 25 && i === 4) {
            if (map.map[i][j].object) map.map[i][j].object.next = new Object("wave", "src/object-wave-up.png");
            if (!map.map[i][j].object) map.map[i][j].object = new Object("wave", "src/object-wave-up.png");
        }
        if (i > 3 && i < 24 && j === 24) {
            if (map.map[i][j].object) map.map[i][j].object.next = new Object("wave", "src/object-wave-right.png");
            if (!map.map[i][j].object) map.map[i][j].object = new Object("wave", "src/object-wave-right.png");
        }
        if (j > 4 && j < 25 && i === 23) {
            if (map.map[i][j].object) map.map[i][j].object.next = new Object("wave", "src/object-wave-down.png");
            if (!map.map[i][j].object) map.map[i][j].object = new Object("wave", "src/object-wave-down.png");
        }
        //Drzewka, potworki...
        if (!map.map[i][j].object) {
            map.map[i][j].object = (Math.random() < 0.075 && map.map[i][j].ground.type !== "path") ? new Object("tree", "src/object-tree-img.png") : null;
        }
        map.map[i][j].creature = ((Math.random() < 0.05 && !map.map[i][j].object && map.map[i][j].ground.type !== "water") ? new Creature("creature", "src/creature-img.png", i, j) : null);
    }
}
screen.takeMapPiece(map.getMapPiece(screen.posW, screen.posH, 10, 10));
    //Podpinamy listenery (keydown)
$(document).ready(function() {
    $(document).bind("keydown", function(e) {
        e.preventDefault();
        if (e.which === 38) {
            hero.move("up");
                //document.getElementById("heroImg").setAttribute("src", "src/hero-img-up.png");
        }
        if (e.which === 37) {
            hero.move("left");
                //document.getElementById("heroImg").setAttribute("src", "src/hero-img-left.png");
        }
        if (e.which === 39) {
            hero.move("right");
                //document.getElementById("heroImg").setAttribute("src", "src/hero-img-right.png");
        }
        if (e.which === 40) {
            hero.move("down");
                //document.getElementById("heroImg").setAttribute("src", "src/hero-img-down.png");
        }
    });
});

// 1. Wyczyszczenie starego i narysowanie nowego ekranu
// 2. Wykonanie ruchu (jeśli nastąpił) - czas 485ms
// 3. Zmiana mapPiece przekazanego do screena (jeśli nastąpił ruch)
// 4. Wywołanie funkcji od nowa po 500ms
function animate() {
    screen.printScreen();
    screen.printSmallMap();
    hero.addToScreen();
    hero.equipment.addToScreen();
    screen.addCreatureListeners();
    hero.clearPrompts();
    hero.atkTargetF();
    screen.removeDeadCreatures();
    hero.updateLevel();
    hero.printInfo();
        //aktywujemy ruch potworków
    for (var i = 0; i < screen.width; i++) {
        for (var j = 0; j < screen.height; j++) {
            if (!(screen.screen[i][j].creature instanceof Creature)) {
                screen.screen[i][j].creature = null;
            }
        }
    }
    for (var i = 0; i < screen.width; i++) {
        for (var j = 0; j < screen.height; j++) {
            if (screen.screen[i][j].creature instanceof Creature) {
                screen.screen[i][j].creature.atkHeroF();
                screen.screen[i][j].creature.followHero();
            }
        }
    }
    //Po atakach wyświetlamy otrzymane obrażenia (zsumowane)
    if (hero.damageTakenInTurn > 0) {
        hero.printPrompt(hero.damageTakenInTurn, "red", "white");
        hero.damageTakenInTurn = 0;
    }
    //jeśli należy poruszyć ekran to rusz ekran
    if (screen.doIMove) {
        switch (screen.whereIMove) {
            case "up":
                if (map.map[hero.posW - 1][hero.posH].canIEnter() === false) break;
                screen.moveUp();
                screen.posW--;
                    hero.posW--;
                    break;
            case "down":
                if (!map.map[hero.posW + 1][hero.posH].canIEnter()) break;
                screen.moveDown();
                screen.posW++;
                    hero.posW++;
                    break;
            case "left":
                if (!map.map[hero.posW][hero.posH - 1].canIEnter()) break;
                screen.moveLeft();
                screen.posH--;
                    hero.posH--;
                    break;
            case "right":
                if (!map.map[hero.posW][hero.posH + 1].canIEnter()) break;
                screen.moveRight();
                screen.posH++;
                    hero.posH++;
                    break;
            default:
                throw "error";
        }
        //console.log("Hero: [posW: " + hero.posW + " |posH: " + hero.posH + "]");
    }
    //aktualizuje stan health i jeśli hero martwy to hero.die()
    hero.printHealth();
    if (hero.currentHealth <= 0) {
        hero.die();
        return;
    }
    hero.printMana();
        //dodajemy trochę healtha co ramkę
    if (hero.currentHealth < hero.health) {
        hero.currentHealth += 0.5;
    }
    screen.doIMove = false;
        //wprawiamy grę w ruch
    setTimeout(function() {
        screen.takeMapPiece(map.getMapPiece(screen.posW, screen.posH, 10, 10));
        animate();
    }, 500);
}

animate();
